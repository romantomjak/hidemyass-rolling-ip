# HideMyAss Rolling IP

Provides two, easy to use scripts, that used together allow you to hide your IP address. Current IP address can be obtained from: `/tmp/hma-ipcheck.txt`

## Setting up VPN

Make both scripts executable:
```
sudo chmod +x hma-*.sh
```

Run hma-update.sh in order to install all required dependencies:
```
sudo ./hma-update.sh
```

After you've installed all of the neccessary dependencies you can run:
```
sudo ./hma-run.sh
```

And in about ~3 minutes you will have successfully changed your IP using HideMyAss VPN service.

## Crontab running every X samples

This section will help you to install hma-run.sh as a cron job to run every X minutes/hours/days.

Run every 30 minutes:
```
*/30 * * * * /path/to/your/hma-scripts/hma-run.sh
```

Run every hour:
```
0 * * * * /path/to/your/hma-scripts/hma-run.sh
```

Run every day at 3:30 AM:
```
30 3 * * * /path/to/your/hma-scripts/hma-run.sh
```

You can silence output (if it ever happens :)) by appending `> /dev/null 2>&1` e.g:
```
30 3 * * * /path/to/your/hma-scripts/hma-run.sh > /dev/null 2>&1
```

## OpenVZ setup

You can ignore this section if you're not using OpenVZ.

### Kernel TUN/TAP support

Make sure the tun module has been already loaded on the hardware node:
```
lsmod | grep tun
```

If it is not there, use the following command to load tun module:
```
modprobe tun
```

If that doesn't works I found an alternate way of activating the tun module via insmod. First locate the correct module for OpenVZ kernel:
```
find /lib/modules/ | grep tun.ko
```

Then use insmod with the returned path that suits your OpenVZ kernel, for example:
```
insmod /lib/modules/3.6.9-1-ARCH/kernel/drivers/net/tun.ko
```

For me, openvpn worked ok afterwards

### Granting container an access to TUN/TAP

Allow your container to use the tun/tap device by running the following commands on the host node:
```
$CTID=101

sudo vzctl set $CTID --devnodes net/tun:rw --save

sudo vzctl set $CTID --devices c:10:200:rw --save

sudo vzctl set $CTID --capability net_admin:on --save

sudo vzctl exec $CTID mkdir -p /dev/net

sudo vzctl exec $CTID mknod /dev/net/tun c 10 200

sudo vzctl exec $CTID chmod 600 /dev/net/tun
```

$CTID is your specific container id.

## License

MIT
