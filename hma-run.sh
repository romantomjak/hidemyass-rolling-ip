#!/bin/bash
#
# Created: 2014-10-25 11:35 PM
# Author:  Romans Tomjaks <r.tomjaks@gmail.com>
# URL:     https://bitbucket.org/r00m/hidemyass-rolling-ip
# Version: 1.0
#
# IT'S NOT ADVISABLE TO SWITCH THE IP FASTER THAN 4 - 5 MINUTES WHEN RUNNING
# THIS SCRIPT ON TWO COMPUTERS AT THE SAME TIME; WHEN RUNNING ON A SINGLE
# COMPUTER YOU CAN CHANGE THE IP AS FAST AS 2 MINUTES AND 30 SECONDS.

# (1) check if required binaries are installed
if [ ! -f /usr/bin/curl ] || [ ! -f /usr/sbin/openvpn ] || [ ! -f /etc/openvpn/hmauser.pass ] || [ ! -f /etc/openvpn/hmauser.key ] ; then
   echo "Error: Please run hma-update.sh first."
   exit 1
fi

# (2) change working dir
cd /etc/openvpn/

# (3) remove old ip check file
rm -f /tmp/hma-ipcheck.txt

# (4) disconnect from previous VPN
killall openvpn

# (5) wait a bit more to make sure that the openvpn has been properly disconnected
sleep 240

# (6) choose a random VPN
hma_vpn=$(find . -name "*.ovpn" | shuf -n 1 | cut -c 3-)

# (7) connect
/usr/sbin/openvpn --daemon --config $hma_vpn

# (8) wait for connection to be established
sleep 60

# (9) write current IP
hma_ip=$(curl http://geoip.hidemyass.com/ip/ 2>/dev/null)
echo "$hma_ip" > /tmp/hma-ipcheck.txt
