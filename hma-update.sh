#!/bin/bash
#
# Script for updating HideMyAss OpenVPN configuration files
#
# Created: 2014-10-25 11:35 PM
# Author:  Romans Tomjaks <r.tomjaks@gmail.com>
# URL:     https://bitbucket.org/r00m/hidemyass-rolling-ip
# Version: 1.0

TEMP=/tmp/temp.$$
trap 'rm -f $TEMP; exit 0' 0 1 2 3 15

show_progress()
{
    while [ -r $TEMP ] ; do
        echo -n .
        sleep 2
    done
}

echo "This script will download/update your HideMyAss OpenVPN configuration files."
read -p "Are you sure you wish to continue? (y/n) " RESP

if [ "$RESP" = "y" ]; 
then

# (1) check if required binaries are installed
if [ ! -f /usr/bin/curl ] || [ ! -f /usr/bin/wget ] || [ ! -f /usr/sbin/openvpn ] || [ ! -f /bin/sed ] ; then
   cat <<EOF
Error: Please install curl, wget, sed and openvpn for HideMyAss Rolling IP to work.
You can try any of the following commands:
apt-get install curl wget sed openvpn
yum install curl wget sed openvpn
EOF
   exit 1
fi

# (3) prompt user, and read credentials from the command line
read -p "Your HMA! username: " username
stty -echo
read -p "Your HMA! password: " password
stty echo

# (4) download process begins
echo
echo -n "Downloading "
touch $TEMP
show_progress &

rm -rf /etc/openvpn/*.*

# (5) write credentials to /etc/openvpn/hmauser.pass
echo -e "$username\n$password" > /etc/openvpn/hmauser.pass

wget --quiet -r -A.ovpn -nd --no-parent https://www.hidemyass.com/vpn-config/TCP/ -P /etc/openvpn/
wget --quiet http://hidemyass.com/vpn-config/keys/ca.crt http://hidemyass.com/vpn-config/keys/hmauser.crt http://hidemyass.com/vpn-config/keys/hmauser.key -P /etc/openvpn/

rm -f $TEMP

echo " done!"
echo "You can find the files in /etc/openvpn/"
sleep 1
echo "Credentials stored in /etc/openvpn/hmauser.pass"
sleep 1
echo "Applying settings..."
sleep 4

# (6) edit all ovpn config files and make them read credentials from a file
sed -i 's/auth-user-pass/auth-user-pass hmauser.pass/g' /etc/openvpn/*
echo "All done!"

else
echo "Good bye!"
fi
